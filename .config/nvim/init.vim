set nu

set tabstop=3
set shiftwidth=3
set cindent

set termguicolors
colorscheme koehler

set mouse=a

nmap <C-l> :tabnext<CR>
nnoremap <ESC><ESC> :noh<CR>
