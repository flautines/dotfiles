import XMonad
import XMonad.Layout
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Layout.NoBorders
import XMonad.Layout.Gaps
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys)
import System.IO
import XMonad.Layout.Spacing

main = do
   xmproc <- spawnPipe "xmobar ~/.xmonad/xmobarrc"
   xmonad $ def {
   modMask     = mod4Mask,
   terminal    = "urxvt",
   manageHook  = composeOne [ isFullscreen -?> doFullFloat,
                              isDialog     -?> doCenterFloat ]
   <+> manageDocks,
      layoutHook		    = avoidStruts $ spacingRaw True (Border 0 5 5 5) True (Border 5 5 5 5) True $ layoutHook def,
      handleEventHook		= handleEventHook def <+> docksEventHook,  
      focusedBorderColor	= "#bbc5ff",
      normalBorderColor		= "#292d3e",
      logHook = dynamicLogWithPP $ xmobarPP
      { ppOutput = hPutStrLn xmproc,
        ppTitle  = xmobarColor "white" "" . shorten 50
      }
   } `additionalKeys` [
      ]
